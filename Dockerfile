FROM node:13-buster

RUN apt-get update \
    && apt-get clean \
    && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false -o APT::AutoRemove::SuggestsImportant=false \
    && rm -rf /var/lib/apt/lists/*; \
    npm i npm@latest -g; \
    mkdir /opt/how_i_code

COPY ./server /opt/how_i_code/server

WORKDIR /opt/how_i_code/server

RUN chown -R node:node /opt/how_i_code

USER node

RUN npm install --no-optional && npm cache clean --force

ENV PATH /opt/app/node_modules/.bin:$PATH
