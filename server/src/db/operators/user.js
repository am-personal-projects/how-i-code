import bluebird from "bluebird";

import { genHash, verifyHash } from "../../auth";
import Operator from "./operator";
import UserModel from "../models/user";

export default class UserOperator extends Operator {
  constructor(queryConfig) {
    super(UserModel, queryConfig);
  }

  create(schemaConfig) {
    // Define a query looking for a user with either the 
    // provided email or username
    const query = {
      $or: [
        { email: schemaConfig.email },
        { username: schemaConfig.username }
      ]
    };

    return new bluebird.Promise(async (resolve, reject) => {
      const queryResult = await super.findOne({ query })
        .catch(reject);

      if (queryResult.success !== null) {
        return reject("User exists");
      }

      const hash = await genHash(schemaConfig.passkey);

      if ("error" in hash) {
        return reject({
          code: hash.error.code,
          from: "argon2"
        });
      }

      schemaConfig.passkey = hash.success;

      const newUser = await super.create(schemaConfig);

      return ("error" in newUser)
        ? reject(newUser)
        : resolve(newUser);
    });
  }

  validateUser(schemaConfig) {
    const query = {
      $or: [
        { email: schemaConfig.email || "" },
        { username: schemaConfig.username || "" }
      ]
    };

    return new bluebird.Promise(async (resolve, reject) => {
      const user = await super.findOne({ query });
      
      if ("error" in user) {
        return reject(user.error);
      }
      
      const validHash = await verifyHash(user.success.passkey, schemaConfig.passkey);

      if ("error" in validHash) {
        return reject(validHash.error);
      }

      return resolve({ success: user.success });
    });
  }
}

