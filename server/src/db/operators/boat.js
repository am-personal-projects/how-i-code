import Operator from "./operator";

import BoatModel from "../models/boat";

export default class CarOperator extends Operator {
  constructor(queryConfig) {
    super(BoatModel, queryConfig);
  }

  async findByHin(hin) {
    const query = { hin }
    return await super.find({ query });
  } 
}
