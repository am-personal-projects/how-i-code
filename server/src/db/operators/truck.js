import VehicleOperator from "./vehicle";

import TruckModel from "../models/truck";

export default class TruckOperator extends VehicleOperator {
  constructor(queryConfig) {
    super(TruckModel, queryConfig);
  }
}
