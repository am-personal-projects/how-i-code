import mongoose from "mongoose";

// Set up generic database operations, providing
// default values for query options, but also allowing them to
// be overwritten as necessary
export default class Operator {

  constructor(model, queryConfig) {
    // Keep the actual config data in an independant structure,
    // this allows us easily replace the default config 
    // with a temperary one.
    this.config = new QueryConfig(queryConfig);

    // Model to perform operations on
    this.model = model;
  }

  /*
   * Database operation methods
   */

  create(schemaConfig) {
    const query = new this.model(schemaConfig).save()
    return this.build(query);
  }

  // Remove a document from the database
  // *This does not remove document references
  deleteOne(queryConfig) {
    return this.findOne(queryConfig)
      .then(doc => {
        const query = doc.success.remove()
        return this.build(query);
      })
      .catch(this.error);
  }

  // Remove multiple documents from the database
  // *This does not remove document references
  // Return errors pertain to Model.find() and mongoose.Promise.all()
  // Success value will be an array containing the result of each
  // call to Document.remove()
  deleteMany(tmpQuery) {
    return this.find(tmpQuery)
      .then(docs => {
        docs.map(doc => doc.remove());

        const result = mongoose.Promise.all(docs);
        return this.build(result)
      })
      .catch(this.error);
  }

  // Get all documents matching the query config from the database
  find(queryConfig) {
    const config = this.config.tryModify(queryConfig),
          query = this.model.find(config.query);

    this.applyConfig(query, config);
    return this.build(query);
  }

  // Get one document matching the query config from the database
  findOne(queryConfig) {
    const config = this.config.tryModify(queryConfig),
          query = this.model.findOne(config.query);

    this.applyConfig(query, config);

    return this.build(query);
  }

  // Get one document matching the provided document ID from the database
  findById(id, queryConfig) {
    const config = this.config.tryModify(queryConfig);
    config.query["_id"] = id;
    return this.findOne(config);
  }

  // Update a document by a given document's id
  update(docId, schemaConfig, queryConfig) {
    const config = this.config.tryModify(queryConfig);

    // Ensure the query includes the relevant document id
    config.query["_id"] = docId;

    // Define the initial model query and apply any
    // custom query options
    let query = this.model.findById(config.query._id);
    this.applyConfig(query, config);
    
    // Add a "document save" operation to the data pipeline
    query = query.then(doc => {
        Object.assign(doc, schemaConfig);
        return this.build(doc.save());
      })

    return this.build(query);
  }

  /*
   * Utilities for data pipeline
   */

  // Apply the provided configuration for a given query
  applyConfig(query, queryConfig) {
    queryConfig.limit && query.limit(queryConfig.limit);
    queryConfig.populate && query.populate(queryConfig.populate);
    queryConfig.select && query.select(queryConfig.select);
    queryConfig.sort && query.sort(queryConfig.sort);

    return query
  }

  // Apply the final data pipeline
  build(query) {
    return query.then(this.success).catch(this.error);
  }

  // Return a query error
  error(queryError) {
    return { error: queryError };
  }

  // Return a successful query
  success(queryResult) {
    return { success: queryResult };
  }
}

// Query configuration used by database operators
class QueryConfig {
  constructor(config) {
    // Define default document properties to return
    const select = {
      __v: 0,
      createdAt: 0,
      updatedAt: 0
    };

    // Merge the provided query "select" option with the
    // default, overriding any defaults
    if (config.select) {
      Object.assign(select, config.select);
    }
    
    this.query = config.query || {};
    this.limit = config.limit || "";
    this.populate = config.populate || "";
    this.select = select;
    this.sort = config.sort || "";
  }

  // Return a new query config with options that
  // override the default configuration
  tryModify(config) {
    // Assign the provided config to a clone of the current config
    const tmpConfig = Object.assign(JSON.parse(JSON.stringify(this)), config);

    return new QueryConfig(tmpConfig);
  }
}

