import VehicleOperator from "./vehicle";

import CarModel from "../models/car";

export default class CarOperator extends VehicleOperator {
  constructor(queryConfig) {
    super(CarModel, queryConfig);
  }
}
