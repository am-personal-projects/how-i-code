import Operator from "./operator";

export default class VehicleOperator extends Operator {
  constructor(model, queryConfig) {
    super(model, queryConfig);
  }

  findByVin(vin) {
    const query = { vin };
    return super.find({ query });
  }
}
