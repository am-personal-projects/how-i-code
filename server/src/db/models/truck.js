import mongoose from "mongoose";

import AutomobileSchema from "../base_schemas/automobile";

// Create a schema with properties that will be unique to
// documents in the Truck collection
const TruckSchema = new mongoose.Schema({
  bed_length: Number
});

// Copy over AutomobileSchema to extend TruckSchema
TruckSchema.add(AutomobileSchema);

// Create the final model for export
const TruckModel = mongoose.model("Truck", TruckSchema);

export default TruckModel;

