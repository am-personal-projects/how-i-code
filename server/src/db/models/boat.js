import mongoose from "mongoose";

import VehicleSchema from "../base_schemas/vehicle";

// Create a schema with properties that will be unique to
// documents in the Boat collection
const BoatSchema = new mongoose.Schema({
  length: Number,
  width: Number,
  hin: String,
  current_hours: Number
});

// Copy over VehicleSchema to extend BoatSchema
BoatSchema.add(VehicleSchema);

// Index the Boat collection by each document's "hin" property,
// ensuring they are all unique
BoatSchema.index({
    hin: 1
  },
  {
    unique: true,
    collation: {
        locale: "en_US",
    },
    name: "hin_unique"
});

// Create the final model for export
const BoatModel = mongoose.model("Boat", BoatSchema);

export default BoatModel;
