import mongoose from "mongoose";

import RootSchema from "../base_schemas/root";

// Create a schema with properties that will be unique to
// documents in the User collection
const UserSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true
  },
  passkey: {
    type: String,
    required: true,
    select: false
  },
  username: {
    type: String,
    required: true
  }
});

// Copy over RootSchema to extend UserSchema
UserSchema.add(RootSchema);

// Index the User collection by each document's "username" property,
// ensuring they are all unique
UserSchema.index({
    username: 1
  },
  {
    unique: true,
    collation: {
        locale: "en_US",
    },
    name: "username_unique"
});

// Create the final model for export
const UserModel = mongoose.model("User", UserSchema);

export default UserModel;
