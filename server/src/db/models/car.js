import mongoose from "mongoose";

import AutomobileSchema from "../base_schemas/automobile";

// Create the final model for export
const CarModel = mongoose.model("Car", AutomobileSchema);

export default CarModel;

