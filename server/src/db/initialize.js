import mongoose from "mongoose";
import bluebird from "bluebird";


function db_initialization() {
  // Enable debugging in development
  (process.env.NODE_ENV === "DEV") && mongoose.set("debug", true);

  // Connect to the database
  mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true });

    // Use the bluebird promise library instead of native promises,
  // bluebird offers better performance (as of 02/08/2020)
  mongoose.promise = bluebird;

  // Give us some basic logging
  mongoose
    .connection
    .once("open", () => console.log("Database connection successful"))
    .on("error", err => console.error("Mongodb connection error:\n", err));
}

export default db_initialization;
