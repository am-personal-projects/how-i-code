import mongoose from "mongoose";

import RootSchema from "./root";

// Create a schema with properties that will be unique to
// documents extending the vehicle schema
const VehicleSchema = new mongoose.Schema({
  make: {
    type: String,
    required: true
  },
  model: {
    type: String,
    required: true
  },
  year: {
    type: Number,
    required: true
  },
  service_interval: {
    type: Number,
    required: true
  }
});

// Copy over the RootSchema to extend VehicleSchema
VehicleSchema.add(RootSchema);

export default VehicleSchema;
