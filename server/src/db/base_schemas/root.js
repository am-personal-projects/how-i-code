import mongoose from "mongoose";

// Define the root schema that all others will extend from

const definition = {};

const options = {
  timestamps: true
};

const methods = {};

const RootSchema = new mongoose.Schema(definition, options, methods);

export default RootSchema;
