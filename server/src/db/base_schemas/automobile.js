import mongoose from "mongoose";

import VehicleSchema from "./vehicle";

// Create a schema with properties that will be unique to
// documents extending the automobile schema
const AutomobileSchema = new mongoose.Schema({
  color: {
    type: String,
    required: true
  },
  current_mileage: {
    type: Number,
    required: true
  },
  seats: {
    type: Number,
    required: true
  },
  vin: {
    type: String,
    required: true
  }
});

// Copy over VehicleSchema to extend AutomobileSchema
AutomobileSchema.add(VehicleSchema);

// Compute "next_service" to ensure it is always correct
AutomobileSchema.virtual("next_service").get(function () {
  return this.current_mileage + this.service_interval;
});

// Index all collections which extend from the automobile schema by
// each document's "vin" property, ensuring they are all unique
AutomobileSchema.index({
    vin: 1
  },
  {
    unique: true,
    collation: {
        locale: "en_US",
    },
    name: "vin_unique"
});

export default AutomobileSchema;
