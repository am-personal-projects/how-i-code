import bodyParser from "body-parser";
import express from "express";
import helmet from "helmet";
import morgan from "morgan";

import initDb from "./db/initialize";
import { AuthRouter, SecureRouter } from "./router/router";
import {userValidation} from "./auth";

initDb();

const app = express();


app.use(morgan("dev"));
app.use(bodyParser.json({limit: "1mb"}));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(helmet());
app.use("/api/auth", AuthRouter());

app.use(userValidation);

app.use("/api", SecureRouter());

process.on("unhandledRejection", err => {
  console.error(err);
});

process.on("uncaughtException", err => {
  console.error(err);
});

export default app;

