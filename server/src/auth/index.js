import argon2 from "argon2";
import bluebird from "bluebird";
import jsonwebtoken from "jsonwebtoken";

import UserOperator from "../db/operators/user";

export async function genHash(password) {
  return argon2.hash(password, {
      type: argon2.argon2id,
      parallelism: 8
    })
    .then(success)
    .catch(error)
}

export function genJWT(data, options) {
  return new bluebird.Promise((resolve, reject) => {
    jsonwebtoken.sign(data, process.env.JWT_SECRET, options, (err, token) => {
      if (err) {
        return reject(error(err));

      } else {
        return resolve(success(token));
      }
    });
  });
}

export async function verifyHash(hash, password) {
  return await argon2.verify(hash, password)
    .then(success)
    .catch(error);
}

export function verifyJWT(token) {
  return new bluebird.Promise((resolve, reject) => {
    jsonwebtoken.verify(token, process.env.JWT_SECRET, function(err, decoded) {
      if (err) {
        return reject(error(err));

      } else {
        return resolve(success(decoded));
      }
    });
  });
}

export async function userValidation(req, res, next) {
  const error = { error: "Invalid credentials" };
  if (!("authorization" in req.headers)) {
    return res.status(401).json(error);
  }

  const token = await verifyJWT(req.headers.authorization.split(" ").pop());

  if ("error" in token) {
    return res.status(401).json(token.error);
  }

  const operator = new UserOperator({});

  return await operator.findById(token.success.id)
    .then(_ => next())
    .catch(_ => res.status(401).json(error));
}

function success(data) {
  return { success: data };
}

function error(data) {
  return { error: data.toString() };
}
