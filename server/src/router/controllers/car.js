import RootController from "./root";
import CarOperator from "../../db/operators/car";

export default class CarController extends RootController {
  constructor(router) {
    super(new CarOperator({}));

    router.route("/cars")
      .get(super.getAll.bind(this))
      .post(super.create.bind(this));

    router.route("/cars/:id")
      .get(super.getSingle.bind(this))
      .delete(this.deleteOne.bind(this))
      .put(super.update.bind(this));
  }
}

