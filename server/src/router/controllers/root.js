export default class RootController {
  constructor(operator) {
    this.operator = operator;
  }

  async create(req, res) {
    const result = await this.operator.create(req.body);
    return this.respond(res, result);
  }

  async getAll(req, res) {
    const result = await this.operator.find();
    return this.respond(res, result);
  }

  async getSingle(req, res) {
    const result = await this.operator.findById(req.params.id.toString());
    return this.respond(res, result);
  }

  async update(req, res) {
    const docId = req.params.id.toString(),
          result = await this.operator.update(docId, req.body);
    return this.respond(res, result);
  }

  async deleteOne (req, res) {
    const result = await this.operator.deleteOne({ _id: req.params.id.toString() })
    this.respond(res, result);
  }

  respond(res, result) {
    if ("success" in result) {
      return res.status(200).json(result)

    } else if ("error" in result) {
      return res.status(500).json(result);

    } else {
      console.error("Result not formatted: ", result);
      return res.status(500).json({ error: "Unable to determine operation resolution status" })
    }

  }
}

