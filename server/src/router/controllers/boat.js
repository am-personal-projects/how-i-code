import RootController from "./root";
import BoatOperator from "../../db/operators/boat";

export default class BoatController extends RootController {
  constructor(router) {
    super(new BoatOperator({}));

    router.route("/trucks")
      .get(super.getAll.bind(this))
      .post(super.create.bind(this));

    router.route("/trucks/:id")
      .get(super.getSingle.bind(this))
      .delete(this.deleteOne.bind(this))
      .put(super.update.bind(this));
  }
}

