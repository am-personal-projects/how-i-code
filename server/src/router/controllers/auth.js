import RootController from "./root";
import UserOperator from "../../db/operators/user";
import {genJWT} from "../../auth";

export default class AuthController extends RootController {
  constructor(router) {
    super(new UserOperator({}));

    router.route("/new-user")
      .post(this.create.bind(this)); 

    router.route("/login")
      .post(this.login.bind(this))
  }

  async create(req, res, next) {
    return await this.operator.create(req.body)
      .then(success => res.status(200).json({ success }))
      .catch(error => res.status(500).json({ error }));
  }

  async login(req, res, next) {
    const error = { error: "Invalid login credentials" };

    if (!req.body.passkey) {
      return res.status(422).json({ error });
    }

    const user = await this.operator.validateUser(req.body);

    if ("error" in user) {
      return res.status(500).json(user);
    }

    const jwt = await genJWT({ id: user.success._id }, { expiresIn: "12h" });

    if ("error" in jwt) {
      return res.status(500).json(jwt);
    }

    return res.status(200).json(jwt);
  }
}

