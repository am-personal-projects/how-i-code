import RootController from "./root";
import TruckOperator from "../../db/operators/truck";

export default class TruckController extends RootController {
  constructor(router) {
    super(new TruckOperator({}));

    router.route("/trucks")
      .get(super.getAll.bind(this))
      .post(super.create.bind(this));

    router.route("/trucks/:id")
      .get(super.getSingle.bind(this))
      .delete(this.deleteOne.bind(this))
      .put(super.update.bind(this));
  }
}

