import express from "express";

import CarController from "./controllers/car";
import TruckController from "./controllers/truck";
import BoatController from "./controllers/boat";
import AuthController from "./controllers/auth";

const secureRoutes = [ CarController, TruckController, BoatController ];

export function AuthRouter() {
  const router = express.Router();
  new AuthController(router);
  return router;
}

export function SecureRouter() {
  const router = express.Router();

  secureRoutes.forEach(Controller => new Controller(router))

  return router;
}
