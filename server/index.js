#!/usr/bin/env node

import cluster from "cluster";
import { createServer } from "http";
import { cpus } from "os";

import app from "./src/app";

const prodMode = process.env.NODE_ENV === "PROD";

if (cluster.isMaster && prodMode) {
  // Enable node clustering in production

  masterProcessInstructions();

} else {
  // Otherwise just start one server

  setupServer();
}

function masterProcessInstructions() {
  // Create a server instance for each cpu thread
  while (cpus().length > 0) {
    let worker = cluster.fork().on("online", () => {
      console.log(`Worker ${worker.process.pid} started`)
    });
    cpus--;
  }

  // Replace workers as they die, full Carnegie mode
  cluster.on("exit", (worker, code, signal) => {
    console.warn(`Worker ${worker.process.pid} died \nCode: ${code} \nSignal: ${signal}`);
    console.warn("Autopsy: ", worker);
    console.warn("Spawning replacement worker...");

    cluster.fork().on("online", () => {
      console.warn(`Replaced worker ${worker.process.pid}`)
    });
  });
}

// Create a server instance, providing a port for the app and server
function setupServer() {
  const port = parseInt(process.env.SERVER_PORT, 10);

  app.set("port", port);

  const server = createServer(app);

  server.listen(port, () => {
    console.log(`Server listening on port ${port}`);
  });
}

