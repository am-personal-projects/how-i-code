#### Environment Variables Needed:
- NODE_ENV `string(DEV || PROD || TEST)`
- SERVER_PORT `number`
- JWT_SECRET `string`
- MONGO_INITDB_ROOT_USERNAME `string`
- MONGO_INITDB_ROOT_PASSWORD `string`
- MONGO_INITDB_DATABASE `string`
- MONGO_URI `string(mongodb://$MONGO_INITDB_ROOT_USERNAME@$MONGO_INITDB_ROOT_PASSWORD@database:27017/$MONGO_INITDB_DATABASE?authSource=admin)`
